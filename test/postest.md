เอกสารข้อกำหนด
============

เอกสารฉบับนี้แสดงตัวอย่างการเขียนข้อกำหนด เพื่อทดสอบ การทำงาน โปรแกรม POS-ON-CLOUD

    ใช้ Sikuli

ข้อกำหนดที่ 1. เรื่องทดสอบการ Run-app
---------------------------------------------

กำหนดให้ความเหมือนของการเปรียบเทียบอยู่ที่ระดับ 90%

    ความเหมือน 0.9

เมื่อเปิดโปรแกรม powershell

    หยุด 3 วินาที
    doubleClick "powershell"
    หยุด 5 วินาที

และพิมพ์ข้อความ เพื่อไปยัง path โปรแกรม

    พิมพ์ "e:\n"
    หยุด 1 วินาที
    พิมพ์ "cd Code\n"
    หยุด 1 วินาที
    พิมพ์ "cd projectstudent\n"
    หยุด 1 วินาที
    พิมพ์ "cd pos-project\n"
    หยุด 1 วินาที

แล้วควรจะเจอข้อความ ตามรูปนี้ ![](path_ของโปรแกรม.png)

    เจอ "path_ของโปรแกรม"

เมื่อเจอแล้วให้พิมพ์ข้อความดังนี้เพื่อ เริ่มทำการ run application

    พิมพ์ "grails run-app\n"
    หยุด 35 วินาที

แล้วจะเจอข้อความตามรูปนี้ ![](ภาพหลังสั่ง_run.png)

    เจอ "ภาพหลังสั่ง_run"

ข้อกำหนดที่ 2. เรื่องทดสอบการใช้งานหน้าล๊อกอิน
---------------------------------------------

เมื่อเปิดโปรแกรม firefox

    เปิด "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
    หยุด 5 วินาที

และทดสอบการทำงาน

    คลิก "กดไปที่URL"
    หยุด 1 วินาที
    พิมพ์ "http://localhost:8080/pos-project/login.zul\n"
    หยุด 5 วินาที

แล้วจะเจอ ส่วนที่ใช้ในการ login ![](หน้าล๊อกอิน.png)

    เจอ "หน้าล๊อกอิน"

เมื่อเจอหน้าล๊อกอินแล้ว ก็ทำการทดสอบหน้าล๊อกอิน โดยทำการ ป้อนข้อมูลใส่ในช่อง ![](username.png) และ ![](passwords.png)

    คลิก "username"
    พิมพ์ "test"
    หยุด 1 วินาที
    คลิก "passwords"
    พิมพ์ "test"
    หยุด 1 วินาที
    คลิก "ปุ่มLogin"
    หยุด 1 วินาที

แล้วจะเห็นข้อความแสดง การล๊อกอิน ที่ไม่สำเร็จ ดังรูป ![](loginfails.png)

    เจอ "loginfalis"

เมื่อทดสอบว่า login fails ทำงานได้ตามปกติ ต่อไปก็ทดสอบ การล๊อกอินเพื่อเข้าใช้งาน

    doubleClick "username"
    พิมพ์ "admin"
    หยุด 1 วินาที
    doubleClick "passwords"
    พิมพ์ "admin"
    หยุด 1 วินาที
    คลิก "ปุ่มLogin"
    หยุด 1 วินาที

แล้วจะเห็นว่า การล๊อกอิน ประสบความสำเร็จ ดังรูปภาพนี้ ![](ภาพหลังล๊อกอิน.png)

    เจอ "ภาพหลังล๊อกอิน"

ข้อกำหนดที่ 3. เรื่องทดสอบการเพิ่มข้อมูลในหัวข้อใบเสร็จ
---------------------------------------------

เมื่อทำการล๊อกอินเสร็จแล้วเสร้จแล้ว ให้คลิกที่ ![](เมนูรายการ.png)

    เจอ "เมนูรายการ"
    คลิก "เมนูรายการ"
    หยุด 4 วินาที
    เจอ "ตั้งค่าใบเสร็จ"
    คลิก "ตั้งค่าใบเสร็จ"
    หยุด 4 วินาที
    เจอ "หน้าตั้งค่าใบเสร็จ"

และทดสอบป้อนข้อมูลเพื่อทดสอบการทำงานดังต่อไปนี้

    คลิก "ช่องชื่อบริษัท"
    พิมพ์ "บริษัทกำจัดผี จำกัด(มหาชน)"
    หยุด 1 วินาที
    คลิก "ช่องสถานที่ตั้ง"
    พิมพ์ "420/2 หมู่ 5 ต.สุรนารี อ.เมือง จ.นครราชสีมา 30000"
    หยุด 1 วินาที
    คลิก "ช่องเลขประจำตัวผู้เสียภาษี"
    พิมพ์ "1419900230161"
    หยุด 1 วินาที
    คลิก "ปุ่มอัพโหลดโลโก้บริษัท"
    หยุด 1 วินาที
    คลิก "pathรูป"
    พิมพ์ "E:\\Code\\projectstudent\\pos-project\\web-app\\ext\\images\n"
    หยุด 1 วินาที
    คลิก "โลโก้บริษัท"
    คลิก "ปุ่มopen"
    หยุด 1 วินาที

แล้วจะเจอภาพที่อัพโหลดมาแสดงบนหน้า โปรแกรม ![](โลโก้ที่อัพโหลด.png)

    เจอ "โลโก้ที่อัพโหลด"
    คลิก "ปุ่มบันทึกการตั้งค่าใบเสร็จ"
    หยุด 3 วินาที
    คลิก "ปุ่มOK"

ข้อกำหนดที่ 4. เรื่องทดสอบการเพิ่มกลุ่มพนักงานใหม่
---------------------------------------------

เมื่อต้องการเพิ่มกลุ่มพนักงานใหม่ให้คลิก ![](เมนูบุคคล.png)

    เจอ "เมนูบุคคล"
    คลิก "เมนูบุคคล"
    หยุด 2 วินาที
    เจอ "หน้าเพิ่มกลุ่มพนักงานใหม่"
    คลิก "ช่องใส่ชื่อกลุ่มพนักงาน"
    พิมพ์ "พนักงานขาย"
    หยุด 1 วินาที
    คลิก "เลือกสิทธิ์ขายได้"
    คลิก "ปุ่มบันทึกข้อมูล"
    หยุด 2 วินาที

แล้วจะเจอ Row ใหม่ของพนักงานขายปรากฏขึ้นมา ![](แถวพนักงานขายบนตาราง.png)

    เจอ "แถวพนักงานขายบนตาราง"

ข้อกำหนดที่ 5. เรื่องทดสอบการเพิ่มกลุ่มพนักงานใหม่อีกครั้ง
---------------------------------------------

เมื่อต้องการเพิ่มกลุ่มพนักงานใหม่ให้ใช้งานได้หลายสิทธิ์

    คลิก "ช่องใส่ชื่อกลุ่มพนักงาน"
    พิมพ์ "เจ้าของร้าน"
    หยุด 2 วินาที
    คลิก "เลือกสิทธิ์ขายได้"
    คลิก "เพิ่มหมวดสินค้าได้"
    คลิก "เพิ่มหมวดพนักงานได้"
    คลิก "ดูยอดได้"
    คลิก "สร้างบัตรส่วนลดได้"
    คลิก "ปุ่มบันทึกข้อมูล"
    หยุด 2 วินาที

แล้วจะเจอ Row ใหม่แสดงขึ้นมาเป็นกลุ่มเจ้าของร้าน ![](กลุ่มเจ้าของร้าน.png)

    เจอ "กลุ่มเจ้าของร้าน"

ข้อกำหนดที่ 6. เรื่องทดสอบการเพิ่มพนักงานใหม่
---------------------------------------------

เมื่อทำการเพิ่มกลุ่มพนักงานเสร็จสิ้นก็ทำการเพิ่มพนักงานใหม่ โดยกดตามรูป ![](เพิ่มพนักงานใหม่.png)

    เจอ "เพิ่มพนักงานใหม่"
    คลิก "เพิ่มพนักงานใหม่"
    หยุด 3 วินาที
    เจอ "หน้าเพิ่มพนักงานใหม่"
    คลิก "ช่องใส่รหัสพนักงาน"
    พิมพ์ "B5200740"
    หยุด 1 วินาที
    คลิก "ช่องใส่รหัสผ่าน"
    พิมพ์ "7412374123"
    หยุด 1 วินาที
    คลิก "ช่องกรอกชื่อพนักงาน"
    พิมพ์ "ธนพล โสดาวิชิต"
    หยุด 1 วินาที
    คลิก "ช่องเลขประจำตัวประชาชน"
    พิมพ์ "1419900230161"
    หยุด 1 วินาที
    คลิกที่ "145px" ด้านขวาของ "กลุ่มพนักงาน"
    คลิก "เลือกกลุ่มพนักงานขาย"
    คลิก "ช่องกรอกที่อยู่"
    พิมพ์ "47/1 หมู่ 6 ถนนวิญญู ต.ศรีสุทโธ อ.บ้านดุง จ.อุดรธานี 41190"
    หยุด 1 วินาที
    คลิก "ช่องกรอกเบอร์โทรศัพท์"
    พิมพ์ "0877740516"
    หยุด 1 วินาที
    คลิก "ช่องกรอกอื่นๆ"
    พิมพ์ "ไม่มี ทดสอบ พิมพ์"
    หยุด 1 วินาที
    คลิก "ปุ่มบันทึกลูกค้าใหม่"
    หยุด 3 วินาที

แล้วจะเจอหน้าเพิ่มกลุ่มพนักงานหลักจากบันทึกข้อมูลลูกค้าเสร็จ

    เจอ "หน้าเพิ่มกลุ่มพนักงานใหม่"

ข้อกำหนดที่ 7. เรื่องทดสอบการแก้ไขข้อมูลพนักงานใหม่
---------------------------------------------

เมื่อเพิ่มพนักงานใหม่เรียบร้อยก็ทดสอบแก้ไขรายละเอียดพนักงานใหม่ดู

    คลิก "เพิ่มพนักงานใหม่"
    เจอ "หน้าเพิ่มพนักงานใหม่"
    คลิกที่ "100px" ด้านขวาของ "เลือกกลุ่มแก้ไขพนักงานใหม่"
    คลิก "เลือกกลุ่มพนักงานขายแก้ไข"
    คลิก "เลื่อนสกอบาร์"
    เจอ "รายการพนักงานใหม่ที่เพิ่มเข้าไป"
    คลิก "กดปุ่มแก้ไข"
    เจอ "รายละเอียดของพนักงาน"
    หยุด 2 วินาที
    ดับเบิลคลิก "ช่องกรอกเบอร์โทรศัพท์ที่บันทึกไว้"
    พิมพ์ "0877777777"
    หยุด 1 วินาที
    คลิก "ปุ่มบันทึกลูกค้าใหม่"
    หยุด 3 วินาที
    เจอ "หน้าเพิ่มกลุ่มพนักงานใหม่"
    คลิก "เพิ่มพนักงานใหม่"
    เจอ "หน้าเพิ่มพนักงานใหม่"
    คลิกที่ "100px" ด้านขวาของ "เลือกกลุ่มแก้ไขพนักงานใหม่"
    คลิก "เลือกกลุ่มพนักงานขายแก้ไข"
    คลิก "เลื่อนสกอบาร์"
    เจอ "รายการพนักงานใหม่ที่เพิ่มเข้าไป"
    คลิก "กดปุ่มแก้ไข"

แล้วจะเจอข้อมูลที่แก้ไข เบอร์โทรศัพท์ไปแล้ว ![](หลังแก้ไขเบอร์โทร.png)

    เจอ "หลังแก้ไขเบอร์โทร"

ข้อกำหนดที่ 8. เรื่องทดสอบการเพิ่มกลุ่มลูกค้าใหม่
---------------------------------------------

เมื่อทำการตรวจสอบการแก้ไขพนักงานเสร็จแล้วก็ทำการทดสอบเพิ่มกลุ่มลูกค้าใหม่ คลิกที่ ![](ข้อเพิ่มกลุ่มลูกค้าใหม่.png)

    คลิก "ข้อเพิ่มกลุ่มลูกค้าใหม่"
    เจอ "หน้าเพิ่มกลุ่มลูกค้า"
    คลิก "ช่องกรอกชื่อกลุ่มลูกค้า"
    พิมพ์ "ลูกค้า VIP"
    หยุด 1 วินาที
    คลิก "บันทึกกลุ่มลูกค้าใหม่"
    หยุด 2 วินาที
    คลิก "ข้อเพิ่มกลุ่มลูกค้าใหม่"
    หยุด 1 วินาที

แล้วจะเจอข้อมูลกลุ่มลูกค้าใหม่ที่สร้างขึ้น ![](ข้อมุลกลุ่มลูกค้าใหม่.png)

    เจอ "ข้อมุลกลุ่มลูกค้าใหม่"

ข้อกำหนดที่ 9. เรื่องทดสอบการเพิ่มข้อมูลลูกค้าใหม่
---------------------------------------------

เมื่อทำการเพิ่มกลุ่มลูกค้าใหม่แล้วก็ทดลองเพิ่มข้อมุลลูกค้าใหม่คลิกที่ ![](เข้าหน้าเพิ่มข้อมูลลูกค้าใหม่.png)

    คลิก "เข้าหน้าเพิ่มข้อมูลลูกค้าใหม่"
    เจอ "หน้าเพิ่มข้อมูลลูกค้าใหม่"
    คลิก "ช่องรหัสลูกค้า"
    พิมพ์ "0001"
    หยุด 1 วินาที
    คลิก "ช่องรหัสผ่านลูกค้า"
    พิมพ์ "7412374123"
    หยุด 1 วินาที
    คลิก "ช่องชื่อลูกค้า"
    พิมพ์ "ธนพล โสดาวิชิต"
    หยุด 1 วินาที
    คลิก "ช่องเลขประจำตัวลูกค้า"
    พิมพ์ "1419900230161"
    หยุด 1 วินาที
    คลิกที่ "150px" ด้านขวาของ "เลือกกลุ่มลูกค้าที่ต้องการ"
    คลิก "เลือกกลุ่มvip"
    คลิก "ช่องที่อยู่ของลูกค้า"
    พิมพ์ "420/2 หมุ่ 5 ต.สุรนารี อ.เมือง จ.นคราชสีมา 30000"
    หยุด 1 วินาที
    คลิก "ช่องเบอร์โทรศัพท์ลูกค้า"
    พิมพ์ "0877725218"
    หยุด 1 วินาที
    คลิก "ช่องอื่นๆหน้าลูกค้า"
    พิมพ์ "test input data เฉยๆ"
    หยุด 1 วินาที
    คลิก "บันทึกข้อมูลลูกค้า"
    หยุด 3 วินาที

แล้วจะเจอหน้าที่ว่างเปล่าแสดงว่าบันทึกแล้ว ![](ข้อมูลต่างๆถูกเคลีรย์.png)

    เจอ "ข้อมูลต่างๆถูกเคลีรย์"

ข้อกำหนดที่ 10. เรื่องทดสอบการแก้ไขข้อมูลลูกค้า
---------------------------------------------

เมื่อทำการบันทึกข้อมูลลูกค้าเสร็จแล้วก็ทำการทดสอบแก้ไขข้อมูลลูกค้า

    คลิกที่ "100px" ด้านขวาของ "เลือกกลุ่มแก้ไขลูกค้า"
    คลิก "เลือกกลุ่มลูกค้าที่จะแสดง"
    คลิก "เลื่อนสกอร์บาร์ของข้อมูลลูกค้า"
    เจอ "ข้อมูลลูกค้าที่จะแก้ไข"
    คลิก "ปุ่มแก้ไขข้อมูลลูกค้า"
    ดับเบิลคลิก "ทดลองเปลี่ยนชื่อลูกค้า"
    พิมพ์ "จินตนาการ เดาเอา"
    หยุด 1 วินาที
    คลิก "ปุ่มบันทึกลูกค้าใหม่"

แล้วจะเจอข้อมูลต่างๆ ที่ถูกเคลีย ตามรูป ![](ข้อมูลต่างๆถูกเคลีรย์.png)

    เจอ "ข้อมูลต่างๆถูกเคลีรย์"

ข้อกำหนดที่ 11. เรื่องทดสอบการเพิ่มหมวดสินค้า
---------------------------------------------

เมื่อทำการเพิ่มข้อมูลลูกค้าเสร็จแล้วก็ทำการเปิดหมวดสินค้าต่อไป กดที่ ![](เมนูสต๊อกสินค้า.png)

    คลิก "เมนูสต๊อกสินค้า"
    หยุด 2 วินาที
    เจอ "หน้าเพิ่มหมดสินค้า"
    คลิก "ช่องกรอกชื่อหมวดสินค้า"
    พิมพ์ "อาหารกระป๋อง"
    หยุด 1 วินาที
    คลิก "บันทึกหมวดสินค้า"
    หยุด 2 วินาที
    เจอ "รายการหมดสินค้าที่เพิ่มเข้าใหม่"

และหลักจากนั้นลองทำการทดลองลบหมวดสินค้าและเพิ่มเข้าไปใหม่ดู

    คลิก "ปุ่มลบหมวดสินค้า"
    หยุด 2 วินาที
    เจอ "รายการหมวดสินค้าว่างเปล่า"
    คลิก "ช่องกรอกชื่อหมวดสินค้า"
    พิมพ์ "อาหารกระป๋อง"
    หยุด 1 วินาที
    คลิก "บันทึกหมวดสินค้า"
    หยุด 2 วินาที

แล้วจะเจอ รายการแสดงดังรูป ![](รายการหมดสินค้าที่เพิ่มเข้าใหม่.png)

    เจอ "รายการหมดสินค้าที่เพิ่มเข้าใหม่"

ข้อกำหนดที่ 12. เรื่องทดสอบการเพิ่มสินค้าใหม่เข้าในสต๊อก
---------------------------------------------

เมื่อทำการเพิ่มหมวดสินค้าเสร็จสิ้นก็ทำการเพิ่มรายการสินค้าใหม่เข้าในสต๊อก คลิก ![](เพิ่มสินค้าใหม่.png)

    คลิก "เพิ่มสินค้าใหม่"
    หยุด 2 วินาที
    เจอ "หน้าเพิ่มสินค้าใหม่ในสต๊อก"
    พิมพ์ "A0001"
    หยุด 1 วินาที
    คลิก "ช่องชื่อสินค้า"
    พิมพ์ "ปลากระป๋องตราสามแม่ครัว"
    หยุด 1 วินาที
    คลิกที่ "145px" ด้านขวาของ "หมวดของสินค้า"
    คลิก "เลือกหมวดอาหารกระป๋อง"
    คลิก "ช่องราคาทุน"
    พิมพ์ "11"
    หยุด 1 วินาที
    คลิก "ช่องราคาขายปลีก"
    พิมพ์ "18"
    หยุด 1 วินาที
    คลิก "ช่องราคาขายส่ง"
    พิมพ์ "13"
    หยุด 1 วินาที
    คลิก "ช่องจำนวนสินค้า"
    พิมพ์ "100"
    หยุด 1 วินาที
    คลิก "ช่องหน่วยสินค้า"
    พิมพ์ "ป๋อง"
    หยุด 1 วินาที
    คลิก "วันหมดอายุของสินค้า"
    พิมพ์ "18/03/2015"
    หยุด 1 วินาที
    คลิก "ช่องรายละเอียดเพิ่มเติมของสินค้า"
    พิมพ์ "ประกระป๋องตราสามแม่ครัวหลากรสอร่อยดี"
    หยุด 1 วินาที
    คลิก "บันทึกข้อมูลสินค้าใหม่"
    หยุด 3 วินาที

แล้วเจอหน้าเพิ่มสินค้าทำการเคลียร์หน้าจอ ![](หน้าเพิ่มสินค้าเคลียร์หน้าจอ.png)

    เจอ "หน้าเพิ่มสินค้าเคลียร์หน้าจอ"

ข้อกำหนดที่ 13. เรื่องทดสอบการเพิ่มสินค้าใหม่เข้าในสต๊อก
---------------------------------------------

เมื่อทำการเพิ่มสต๊อกสินค้าใหม่ก็ทำการแก้ไขข้อมูลสินค้าดู คลิกที่ ![](ค้นหาหมวดสินค้า.png)

    คลิกที่ "160px" ด้านขวาของ "ค้นหาหมวดสินค้า"
    คลิก "เลือกค้นหาหมวดอาหารกระป๋อง"
    คลิก "เลื่อนสกอร์บารเพื่อดูรายการสินค้า"
    เจอ "รายการสินค้าหมวดอาหารกระป๋อง"
    คลิก "แก้ไขข้อมูลสินค้า"
    คลิก "แกไขราคาขายปลีก"
    พิมพ์ "\b\b18.5"
    หยุด 1 วินาที
    หยุด 1 วินาที
    คลิก "บันทึกข้อมูลสินค้าใหม่"
    หยุด 3 วินาที
    คลิกที่ "160px" ด้านขวาของ "ค้นหาหมวดสินค้า"
    คลิก "เลือกค้นหาหมวดอาหารกระป๋อง"
    คลิก "เลื่อนสกอร์บารเพื่อดูรายการสินค้า"
    คลิก "แก้ไขข้อมูลสินค้า"

แล้วจะเจอรายการสินค้า ![](เจอรายการที่แก้ไข.png)

    เจอ "เจอรายการที่แก้ไข"

ข้อกำหนดที่ 14. เรื่องทดสอบการสร้างบัตรส่วนลด
---------------------------------------------

เมื่อทำกรเพิ่มข้อมูลสินค้าเสร็จสิ้นก็ทำการสร้างบัตรส่วนลด คลิก ![](เมนูรายการ.png)

    คลิก "เมนูรายการ"
    หยุด 2 วินาที
    เจอ "หน้าสร้างบัตรส่วนลด"
    คลิก "กรอกจำนวนเงินส่วนลด"
    หยุด 1 วินาที
    พิมพ์ "100"
    หยุด 1 วินาที
    คลิก "ช่องกรอกจำนวนบัตรส่วนลด"
    หยุด 1 วินาที
    พิมพ์ "10"
    หยุด 1 วินาที
    คลิก "บันทึกการสร้างบัตรส่วนลด"
    หยุด 2 วินาที
    เจอ "หน้าสร้างบัตรส่วนลด"
    คลิกที่ "100px" ด้านขวาของ "เลือกราคาบัตรที่ค้นหา"
    คลิก "เลือกราคา100บาท"
    คลิก "เลื่อนสกอร์บาร์เพื่อดูรายการบัตร"
    เจอ "รายการบัตรทั้งหมดที่ถูกสร้าง"
    คลิก "เลือกทั้งหมด"
    คลิก "ลบบัตรทั้งหมด"
    หยุด 2 วินาที
    คลิก "เมนูรายการ"
    คลิก "กรอกจำนวนเงินส่วนลด"
    หยุด 1 วินาที
    พิมพ์ "100"
    หยุด 1 วินาที
    คลิก "ช่องกรอกจำนวนบัตรส่วนลด"
    หยุด 1 วินาที
    พิมพ์ "10"
    หยุด 1 วินาที
    คลิก "บันทึกการสร้างบัตรส่วนลด"
    หยุด 2 วินาที

แล้วจะเจอหน้าจอที่ทำการเคลียร์ ![](หน้าสร้างบัตรส่วนลด.png)

    เจอ "หน้าสร้างบัตรส่วนลด"

ทำงานเสร็จแล้วปิดโปรแกรม

    ปิด