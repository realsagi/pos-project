เอกสารข้อกำหนด
============

เอกสารฉบับนี้แสดงตัวอย่างการเขียนข้อกำหนด เพื่อทดสอบ การทำงาน โปรแกรม POS-ON-CLOUD

    ใช้ Sikuli

ข้อกำหนดที่ 5. เรื่องทดสอบการเพิ่มกลุ่มพนักงานใหม่อีกครั้ง
---------------------------------------------

กำหนดให้ความเหมือนของการเปรียบเทียบอยู่ที่ระดับ 90%

    ความเหมือน 0.9

เมื่อต้องการเพิ่มกลุ่มพนักงานใหม่ให้ใช้งานได้หลายสิทธิ์

    พิมพ์ "เจ้าของร้าน"
    หยุด 2 วินาที
    คลิก "เลือกสิทธิ์ขายได้"
    คลิก "เพิ่มหมวดสินค้าได้"
    คลิก "เพิ่มหมวดพนักงานได้"
    คลิก "ดูยอดได้"
    คลิก "สร้างบัตรส่วนลดได้"
    คลิก "ปุ่มบันทึกข้อมูล"
    หยุด 2 วินาที

แล้วจะเจอ Row ใหม่แสดงขึ้นมาเป็นกลุ่มเจ้าของร้าน ![](กลุ่มเจ้าของร้าน.png)

    เจอ "กลุ่มเจ้าของร้าน"