เอกสารข้อกำหนด
============

เอกสารฉบับนี้แสดงตัวอย่างการเขียนข้อกำหนด เพื่อทดสอบ การทำงาน โปรแกรม POS-ON-CLOUD

    ใช้ Sikuli

ข้อกำหนดที่ 9. เรื่องทดสอบการเพิ่มข้อมูลลูกค้าใหม่
---------------------------------------------

กำหนดให้ความเหมือนของการเปรียบเทียบอยู่ที่ระดับ 90%

    ความเหมือน 0.9

เมื่อทำการเพิ่มกลุ่มลูกค้าใหม่แล้วก็ทดลองเพิ่มข้อมุลลูกค้าใหม่คลิกที่ ![](เข้าหน้าเพิ่มข้อมูลลูกค้าใหม่.png)

    คลิก "เข้าหน้าเพิ่มข้อมูลลูกค้าใหม่"
    เจอ "หน้าเพิ่มข้อมูลลูกค้าใหม่"
    คลิก "ช่องรหัสลูกค้า"
    พิมพ์ "0001"
    หยุด 1 วินาที
    คลิก "ช่องรหัสผ่านลูกค้า"
    พิมพ์ "7412374123"
    หยุด 1 วินาที
    คลิก "ช่องชื่อลูกค้า"
    พิมพ์ "ธนพล โสดาวิชิต"
    หยุด 1 วินาที
    คลิก "ช่องเลขประจำตัวลูกค้า"
    พิมพ์ "1419900230161"
    หยุด 1 วินาที
    คลิกที่ "130px" ด้านขวาของ "เลือกกลุ่มลูกค้าที่ต้องการ"
    คลิก "เลือกกลุ่มvip"
    คลิก "ช่องที่อยู่ของลูกค้า"
    พิมพ์ "420/2 หมุ่ 5 ต.สุรนารี อ.เมือง จ.นคราชสีมา 30000"
    หยุด 1 วินาที
    คลิก "ช่องเบอร์โทรศัพท์ลูกค้า"
    พิมพ์ "0877725218"
    หยุด 1 วินาที
    คลิก "ช่องอื่นๆหน้าลูกค้า"
    พิมพ์ "test input data เฉยๆ"
    หยุด 1 วินาที
    คลิก "บันทึกข้อมูลลูกค้า"
    หยุด 3 วินาที

แล้วจะเจอหน้าที่ว่างเปล่าแสดงว่าบันทึกแล้ว ![](ข้อมูลต่างๆถูกเคลีรย์.png)

    เจอ "ข้อมูลต่างๆถูกเคลีรย์"