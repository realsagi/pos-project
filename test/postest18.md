เอกสารข้อกำหนด
============

เอกสารฉบับนี้แสดงตัวอย่างการเขียนข้อกำหนด เพื่อทดสอบ การทำงาน โปรแกรม POS-ON-CLOUD

    ใช้ Sikuli

ข้อกำหนดที่ 18. ทดสอบการขายแบบ คิด VAT และ ราคาขายปลีก ยังไม่มีโปรโมชั่น และหลายจำนวน
---------------------------------------------

กำหนดให้ความเหมือนของการเปรียบเทียบอยู่ที่ระดับ 90%

    ความเหมือน 0.9

เมื่อทำกรเพิ่มข้อมูลสินค้าเสร็จสิ้นก็ทำการสร้างบัตรส่วนลด คลิก ![](เมนูหน้าร้าน.png)

    คลิก "เมนูหน้าร้าน"
    หยุด 2 วินาที
    เจอ "หน้าขายสินค้าจะมีคำว่ายอดรวมตัวใหญ่ๆ"
    คลิก "ช่องจำนวนสินค้าที่จะขาย"
    หยุด 1 วินาที
    พิมพ์ "10"
    หยุด 1 วินาที
    คลิก "หน้าขายสินค้าจะมีคำว่ายอดรวมตัวใหญ่ๆ"
    คลิก "ช่องรหัสสินค้าสำหรับขาย"
    พิมพ์ "A0001"
    หยุด 2 วินาที
    เจอ "รายการสินค้าที่เราจะขาย"
    คลิก "จบการขาย"
    หยุด 2 วินาที
    เจอ "หน้ากรอกเงินรับคำนวณเงินทอน"
    คลิก "ช่องชำระเงิน"
    พิมพ์ "200"
    หยุด 1 วินาที
    คลิก "okจบการขาย"
    หยุด 7 วินาที
    เจอ "เมนูที่สามารถกดได้"
    คลิก "พิมพ์ใบเสร็จ"
    หยุด 3 วินาที
    เจอ "ใบเสร็จรับเงิน"
    คลิกที่ "120px" ด้านขวาของ "ปิดใบเสร็จรับเงิน"
    หยุด 2 วินาที
    คลิก "เริ่มขายสินค้าใหม่"
    หยุด 4 วินาที

แล้วจะเจอหน้าจอที่ทำการเคลียร์ ![](หน้าขายสินค้าจะมีคำว่ายอดรวมตัวใหญ่ๆ.png)

    เจอ "หน้าขายสินค้าจะมีคำว่ายอดรวมตัวใหญ่ๆ"