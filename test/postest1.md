เอกสารข้อกำหนด
============

เอกสารฉบับนี้แสดงตัวอย่างการเขียนข้อกำหนด เพื่อทดสอบ การทำงาน โปรแกรม POS-ON-CLOUD

    ใช้ Sikuli

ข้อกำหนดที่ 1. เรื่องทดสอบการ Run-app
---------------------------------------------

กำหนดให้ความเหมือนของการเปรียบเทียบอยู่ที่ระดับ 90%

    ความเหมือน 0.9

เมื่อเปิดโปรแกรม powershell

    หยุด 3 วินาที
    doubleClick "powershell"
    หยุด 5 วินาที

และพิมพ์ข้อความ เพื่อไปยัง path โปรแกรม

    พิมพ์ "e:\n"
    หยุด 1 วินาที
    พิมพ์ "cd Code\n"
    หยุด 1 วินาที
    พิมพ์ "cd projectstudent\n"
    หยุด 1 วินาที
    พิมพ์ "cd pos-project\n"
    หยุด 1 วินาที

แล้วควรจะเจอข้อความ ตามรูปนี้ ![](path_ของโปรแกรม.png)

    เจอ "path_ของโปรแกรม"

เมื่อเจอแล้วให้พิมพ์ข้อความดังนี้เพื่อ เริ่มทำการ run application

    พิมพ์ "grails run-app\n"
    หยุด 35 วินาที

แล้วจะเจอข้อความตามรูปนี้ ![](ภาพหลังสั่ง_run.png)

    เจอ "ภาพหลังสั่ง_run"

ทำงานเสร็จแล้วปิดโปรแกรม

    ปิด