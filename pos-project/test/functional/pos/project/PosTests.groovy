package pos.project

import org.junit.Test
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

class PosTests extends  zk.grails.ComposerTestCase {

    @Test
    void testloginfails() {
        browser.go "/pos-project/login.zul"

        $(".z-textbox")[0].value("test")    
        waitFor { $(".z-textbox")[0].value() == "test" }   

        $(".z-textbox")[1].value("test")    
        waitFor { $(".z-textbox")[1].value() == "test" }   

        $(".z-button")[0].click()

        waitFor { $(".z-label")[0].text() == "ไม่พบ Username test ค่ะ" } 

    } 

    @Test
    void testloginpass(){
        browser.go "/pos-project/login.zul"

        $(".z-textbox")[0].value("admin")    
        waitFor { $(".z-textbox")[0].value() == "admin" }   

        $(".z-textbox")[1].value("admin")    
        waitFor { $(".z-textbox")[1].value() == "admin" }   

        $(".z-button")[0].click()
        waitFor { $(".z-button")[0].text() == "ออกจากระบบ" }
    }

    @Test
    void testaddnewmodestock(){
        testloginpass() 
        browser.go "/pos-project/newmodestock.zul"

        $(".z-textbox")[0].value("อาหารกระป๋อง")    
        waitFor { $(".z-textbox")[0].value() == "อาหารกระป๋อง" }   

        $(".btn-success")[0].click()
        waitFor { $(".z-row-content > .z-label")[1].text() == "อาหารกระป๋อง" } 

        $(".z-textbox")[0].value("เครื่องดื่ม")    
        waitFor { $(".z-textbox")[0].value() == "เครื่องดื่ม" }  

        $(".btn-success")[0].click() 
        waitFor { $(".z-row-content > .z-label")[3].text() == "เครื่องดื่ม" } 
    }  

    @Test
    void testnewstock(){
        testloginpass()
        browser.go "/pos-project/newstock.zul"
        waitFor{$(".z-textbox")[0].value() == ""} 
        
        $(".z-textbox")[0].value("0001")
        waitFor{$(".z-textbox")[0].value() == "0001"}

        $(".z-textbox")[1].value("ปลากระป๋องตราสามแม่ครัว")
        waitFor{$(".z-textbox")[1].value() == "ปลากระป๋องตราสามแม่ครัว"}

        $(".z-combobox-button")[0].click()
        $(".z-combobox-input")[0] << "อาหารกระป๋อง"
        waitFor{ $(".z-combobox-input")[0].value() ==  "อาหารกระป๋อง" }

        $(".z-doublebox")[0].value(10)
        waitFor{ $(".z-doublebox")[0].value() == "10" }

        $(".z-doublebox")[1].value(15)
        waitFor{ $(".z-doublebox")[1].value() == "15" }

        $(".z-doublebox")[2].value(12)
        waitFor{ $(".z-doublebox")[2].value() == "12" }

        $(".z-intbox")[0].value(100)
        waitFor{ $(".z-intbox")[0].value() == "100"}

        $(".z-textbox")[2].value("กระป๋อง")
        waitFor{$(".z-textbox")[2].value() == "กระป๋อง"}

        $(".z-datebox-button")[1].click()
        $(".z-datebox-input")[1] << "30/03/2014"
        waitFor{$(".z-datebox-input")[1].value() == "30/03/2014"}

        $(".z-textbox")[3].value("อื่นๆ")
        waitFor{$(".z-textbox")[3].value() == "อื่นๆ"}

        $(".z-button")[2].click()
        waitFor{$(".z-textbox")[2].value() == ""} 

        $(".z-combobox-input")[1] << "อาหารกระป๋อง"
        $(".z-textbox")[2].click()
        waitFor{    
            $(".z-combobox-input")[1].value() ==  "อาหารกระป๋อง" 
            $(".z-row-content > .z-label")[13].text() == "0001"
        }  
    }

    @Test
    void testaddnewstock(){
        testloginpass()
        browser.go "/pos-project/newstock.zul"
        $(".z-textbox")[0].value("0001")
        $(".z-textbox")[1].click()
        waitFor{    
            $(".z-textbox")[1].value() == "ปลากระป๋องตราสามแม่ครัว"
            $(".btn-default")[0].click()
            $(".z-intbox")[1].value(20)
            $(".z-intbox")[1].value() == "20"
        } 
    }

    @Test
    void testgencard(){
        testloginpass()
        browser.go "/pos-project/gencard.zul"
        $(".z-intbox")[0].value(100)
        waitFor{ $(".z-intbox")[0].value() == "100"}

        $(".z-intbox")[1].value(50)
        waitFor{ $(".z-intbox")[1].value() == "50"}

        $(".z-button")[2].click()
        waitFor{ $(".z-intbox")[0].value() == "0"}

        $(".z-combobox-input")[0] << "100"
        $(".z-combobox-input")[1].click()
        waitFor{ $(".z-combobox-input")[0].value() ==  "" }

    }

    @Test
    void testsetting(){
        testloginpass()
        browser.go "/pos-project/setting.zul"
        waitFor{$(".z-textbox")[0].value() == ""} 
        $(".z-textbox")[0].value("บริษัทกำจัดผี จำกัด (มหาชน)") 
        waitFor{$(".z-textbox")[0].value() == "บริษัทกำจัดผี จำกัด (มหาชน)"}
        $(".z-textbox")[1].value("420/2 หมู่ 5 ตำบล สุรนารี อำเภอ เมือง จังหวัด นครราชสีมา 30000") 
        waitFor{$(".z-textbox")[1].value() == "420/2 หมู่ 5 ตำบล สุรนารี อำเภอ เมือง จังหวัด นครราชสีมา 30000"} 
        $(".z-textbox")[2].value("1419900230161") 
        waitFor{$(".z-textbox")[2].value() == "1419900230161"} 

    }
}
