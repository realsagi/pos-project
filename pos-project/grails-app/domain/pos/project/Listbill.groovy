package pos.project

class Listbill {
	String billid
	String usersale
	String namecustomer
	String date

	String emid
	String barcode
	String nameproduct
	int countpro
	Double priceperunit
	Double pricebig
	Double sum

	Double discount
	Double total
	Double cash
	Double change
	Double vat

    static constraints = {
    }
}
