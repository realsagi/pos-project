package pos.project


class IndexComposer extends zk.grails.Composer {

    def afterCompose = { window ->
        int count = 0
        boolean clickalert = true
        boolean onclick = false
        boolean checkprin = false
        def checksetting = Infosetting.findAll()
        if("${checksetting}" == "[]"){
            $d('#statesetting').append{
                label(value:"เพื่อการทำงานที่สมบูรณ์ กรุณา กรอกรายละเอียดเกี่ยวกับบริษัท")
                $('#pageclick').setVisible(true)
            }
        }
        else{
            $('#pageclick').setVisible(false)
        }
        $d('#timer').on('timer',{
            for(Newstock searchproduct : Newstock.findAll()){
                if(searchproduct.countproduct <= searchproduct.alert_count && clickalert == true){
                    count++
                    $d('#alertbut').setLabel("${count}")
                    if(checkprin == true){
                        $d('#listalert > rows > row').detach()
                        checkprin = false
                    }
                    if(checkprin == false){
                        $d('#listalert > rows').append{
                            row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
                                label(value:"${count}",style:"font-size:14px;font-weight:bold;color:black")
                                label(value:searchproduct.nameproduct,style:"font-size:14px;font-weight:bold;color:black")
                                label(value:"${searchproduct.countproduct}",style:"font-size:14px;font-weight:bold;color:black")
                                label(value:searchproduct.nameunit,style:"font-size:14px;font-weight:bold;color:black")
                            }
                        }
                    }
                }
            }
            checkprin = true
            clickalert = false
        })
        $d('#alertbut').on('click',{
            if(onclick == false){
                $d('#alertbut').setImage("/ext/images/icon/up.png")
                $d('#messagealert').setVisible(true)
                $d("#messagealert").setLeft("1%")
                $d("#messagealert").setTop("15%")
                $d("#messagealert").setSizable(true)
                $d("#messagealert").doPopup() 
                clickalert = true
                count = 0
                onclick = true
            }
            else if(onclick == true){
                $d('#alertbut').setImage("/ext/images/icon/down.png")
                $d('#messagealert').setVisible(false)
                onclick = false
            }
        })
        $('#useronline').val(session.user)
    	$('#groupuse').val(session.groupname)
    	$('#logout').on('click',{
    		def del = Stamplogin.findByUser(session.user)
    		if(del != null){
    			session.user = ""
    			session.groupname = ""
    			session.l1 = ""
	    		session.l2 = ""
	    		session.l3 = ""
	    		session.l4 = ""
	    		session.l5 = ""
    			del.delete()
    			redirect(uri: "/login.zul")
    		}
    	})
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            session.l1 = "ขายได้"
            session.l2 = "เพิ่มหมวดและสินค้าได้"
            session.l3 = "เพิ่มหมวดและพนักงานได้"
            session.l4 = "ดูยอดและใบเสร็จย้อนหลังได้"
            session.l5 = "สร้างบัตรส่วนลดได้"
        }
        else{
            redirect(uri: "/login.zul")
        } 
    }
}
