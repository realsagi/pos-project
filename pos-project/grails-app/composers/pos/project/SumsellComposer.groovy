package pos.project

class SumsellComposer extends zk.grails.Composer {

    List<String> billidarray = new ArrayList<String>()
    Boolean checkprint = false
    Double sumall = 0
    Double sumvat = 0

    def afterCompose = { window ->
        billidarray.add("begin")
        //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.l4 == session.l4){
                window.visible = true
            }
            else{
                redirect(uri: "/subindex.zul")
            }
        }
        //=====================================================================

        String stringstate = ""
        for(Listbill m : Listbill.findAll()){
            if(stringstate != m.date.substring(0,10)){
                stringstate = m.date.substring(0,10)
                $('#search').append{
                    comboitem(label:m.date.substring(0,10))
                }
            }
        }

        $('#search').on('select',{
            printrow()
        })
        
        Boolean checkstatecheck = false

        $('#checkall').on('click',{
            if(checkstatecheck == false){
                for(cb in $('#listbuy > rows > row > checkbox[checked=false]')){
                    cb.setChecked(true)
                    $('#checkall').setLabel("ยกเลิกการเลือกทั้งหมด")
                    checkstatecheck = true
                }
            }
            else if(checkstatecheck == true){
                for(cb in $('#listbuy > rows > row > checkbox[checked=true]')){
                    cb.setChecked(false)
                    $('#checkall').setLabel("เลือกทั้งหมด")
                    checkstatecheck = false
                }   
            }
            

        })

        $('#delete').on('click',{
            for(cb in $('#listbuy > rows > row > checkbox[checked=true]')){
                for(Listbill b : Listbill.findAll()){
                    if(cb.getName() == b.billid){
                        b.delete()
                    }
                }
            }
            redirect(uri: "/sumbuy.zul")
        })
    }

    public void printrow(){
        if(checkprint == true){
            $('#listbuy > rows > row').detach()
            checkprint = false
            sumall = 0
            sumvat = 0
            billidarray.clear()
        }
        for(Listbill b : Listbill.findAll()){
            if($('#search').text() == b.date.substring(0,10)){
                if(checkbillid(b.billid) == true){
                    $('#listbuy > rows').append{
                        row(style:"font-weight:bold;color:black;background:pink;"){
                            label(value:b.date.substring(0,10),style:"font-weight:bold;color:black")
                            label(value:b.date.substring(10,19)+"("+b.billid+")",style:"font-weight:bold;color:black")
                            label(value:b.usersale,style:"font-weight:bold;color:black")
                            label(value:b.namecustomer,style:"font-weight:bold;color:black")
                            label(value:b.total,style:"font-weight:bold;color:black")
                            label(value:b.vat,style:"font-weight:bold;color:black")
                            button(label:"พิมพ์",id:"b"+b.billid,href:"static/ext/pdf/"+b.usersale+b.billid+".pdf",target:"_new")
                            sumall = sumall+b.total
                            sumvat = sumvat+b.vat
                            checkbox(id:b.id,name:b.billid)
                        }
                    }
                    $('#checkall').setDisabled(false)
                    $('#delete').setDisabled(false)
                    checkprint = true
                }
            }
        }
        $('#listbuy > rows').append{
            sumall = twodigit(sumall)
            sumvat = twodigit(sumvat)
            row(spans:"4",style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
                label(value:"รวม",style:"font-size:14px;font-weight:bold;color:red")
                label(value:"${sumall}",style:"font-size:14px;font-weight:bold;color:red")
                label(value:"${sumvat}",style:"font-size:14px;font-weight:bold;color:red")
                label(style:"font-size:14px;font-weight:bold;color:red")
                label(style:"font-size:14px;font-weight:bold;color:red")
            }
            checkprint = true
        }
    }

    public boolean checkbillid(String numbill){
        for (String item : billidarray) {
            if(item == numbill){
                return false
            }
        }
        billidarray.add(numbill)
        return true
    }

    public BigDecimal twodigit(Double num){
        BigDecimal twodi = new BigDecimal(num)
        twodi = twodi.setScale(2,BigDecimal.ROUND_HALF_EVEN)
        return twodi
    }
}
