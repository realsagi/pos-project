package pos.project
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

import java.io.FileOutputStream

import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Font
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.Rectangle
import com.itextpdf.text.Element
import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.Chunk
import com.itextpdf.text.pdf.draw.VerticalPositionMark
import com.itextpdf.text.Image
import com.itextpdf.text.pdf.ColumnText

class SellComposer extends zk.grails.Composer {

	Double tempsumall = 0
    boolean checkprin = false
    Double vat = 0
    String vatornostate = ""

    def afterCompose = { window ->
        //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.l1 == session.l1){
                window.visible = true
            }
            else{
                redirect(uri: "/subindex.zul")
            }
        }
        //=====================================================================
        vatornostate = $('#vatorno').selectedItem.label
        $('#sumprice').val(0)
        $('#idcus').val("0000")
        $('#namecus').val("ลูกค้าทั่วไป")
        $('#idproduct').focus()

        int countpro = Integer.parseInt($('#count').text())

        Date date = new Date()
        $('#imday').val(date)

        $('#count').on('click',{
            $('#count').select()
        })
        $('#count').on('change',{
            countpro = Integer.parseInt($('#count').text())
        })

        $('#idcus').on('click',{
            $('#idcus').select()
        })

        $('#idcus').on('change',{
            def findcus = Customer.findByCusid($('#idcus').text())
            if(findcus != null){
                $('#namecus').val(findcus.namecus)
            }
            else{
                $('#namecus').val("")
            }
        })

        def findem = Employee.findByEmid(session.user)
        if(findem != null){
            $('#idem').val(findem.emid)
            $('#nameem').val(findem.nameem)
        }

        runbill()
        
        printrowsell()
        $('#idproduct').on('change',{
            Double sumtemp = 0
            Double sumall = 0
            def findidbar = Newstock.findByBarcode($('#idproduct').text())

            if(findidbar != null){
                String bar = findidbar.barcode
                String name = findidbar.nameproduct
                Double priceu = findidbar.priceperunit
                Double priceb = findidbar.pricebig
                sumtemp = countpro*findidbar.priceperunit

                boolean checkseartemp = false
                for(Tempsell findtempbar : Tempsell.findAll()){
                    if(findtempbar.barcode == $('#idproduct').text() && session.user == findtempbar.emid){
                        findtempbar.countpro = countpro+findtempbar.countpro
                        findtempbar.priceperunit = priceu
                        findtempbar.pricebig = priceb
                        findtempbar.sum = sumtemp+findtempbar.sum
                        findtempbar.save()

                        printrowsell()
                        $('#idproduct').val("")
                        $('#idproduct').focus()
                        $('#count').val(1)
                        countpro = 1
                        checkseartemp = true
                    }
                }
                if(checkseartemp == false){
                    def savetemp = new Tempsell()
                    savetemp.emid = session.user
                    savetemp.barcode = bar
                    savetemp.nameproduct = name
                    savetemp.countpro = countpro
                    savetemp.priceperunit = priceu
                    savetemp.pricebig = priceb
                    savetemp.sum = sumtemp
                    savetemp.save()

                    printrowsell()
                    $('#idproduct').val("")
                    $('#idproduct').focus()
                    $('#count').val(1)
                    countpro = 1
                    checkseartemp = true
                }
           }
           else{
                printrowsell()
                $('#count').val(1)
                countpro = 1
           }
        })
        
        $('#vatorno').on('click',{
            printrowsell()
        })

        $('#toonorsent').on('click',{
            printrowsell()
        })

        $('#idgift').on('change',{
            Double oldsumall = $('#sumprice').getValue() 
            Double down = Double.parseDouble($('#downpricefromlist').text())
            def findcard = Card.findByIdcard($('#idgift').text())
            if(findcard != null){
                DateFormat  df = new SimpleDateFormat("dd/MM/yyyy")
                Date today = df.parse($('#imday').text())
                Date extcard = df.parse(findcard.extday)
                if(today <= extcard){
                    int downbath = findcard.bath
                    $('#downpricefromlist').val("${downbath}")
                    printrowsell()
                }
                else{
                    alert("บัตรส่วนลดหมดอายุแล้วค่ะ")
                    $('#idgift').val("")
                }
            }
            else{
                $('#downpricefromlist').val("0")
                printrowsell()
            }
        })

        $('#finish').on('click',{
            $d("#finishpage").setVisible(true)
            $d("#finishpage").setLeft("30%")
            $d("#finishpage").setTop("30%")
            $d("#finishpage").setSizable(true)
            $d("#finishpage").doPopup() 
            $d('#sumfinish').val($('#sumprice').getValue())
            $d('#payfinish').focus()
            $d('#payfinish').val(0)
            $d('#tonfinish').val(0)
            $d('#okfinish').setDisabled(true)
            $d('#payfinish').on('change',{
                session.cash = $d('#payfinish').getValue()
                if($d('#payfinish').getValue() == null){
                    $d('#payfinish').val(0)
                }
                else{
                    $d('#tonfinish').val($d('#payfinish').getValue() - $d('#sumfinish').getValue())
                    session.change = twodigit($d('#tonfinish').getValue())
                    if($d('#tonfinish').getValue() >= 0){
                        $d('#okfinish').setDisabled(false)
                    }
                    else{
                        $d('#okfinish').setDisabled(true)
                    }
                }
            $d('#okfinish').on('click',{
                    $d('#printbill').setDisabled(false)
                    $d('#newpage').setDisabled(false)
                    $d("#finishpage").setVisible(false)
                    pdfitext()
                })
            })
        })

        $('#newpage').on('click',{ // เเริ่มการขายสินค้าใหม่
            for(Tempsell temp : Tempsell.findAll()){
                if(temp.emid == session.user){
                    def savebill = new Listbill()

                    savebill.billid = $('#idbill').text()
                    savebill.usersale = session.user
                    savebill.namecustomer = $('#namecus').text()
                    savebill.date = dateandtime()

                    savebill.emid = temp.emid
                    savebill.barcode = temp.barcode
                    savebill.nameproduct = temp.nameproduct
                    savebill.countpro = temp.countpro
                    savebill.priceperunit = temp.priceperunit
                    savebill.pricebig = temp.pricebig
                    savebill.sum = temp.sum

                    savebill.discount =  Double.parseDouble($('#downpricefromlist').text())
                    savebill.total =  Double.parseDouble($('#sumpricefromlist').text().replace(",",""))
                    savebill.cash =  session.cash
                    savebill.change =  session.change
                    savebill.vat =  Double.parseDouble($('#vatpricefromlist').text())
                    savebill.save()

                    def discountstock = Newstock.findByBarcode(temp.barcode)
                    int oldcoutpro = discountstock.countproduct
                    int newcountpro = oldcoutpro - temp.countpro
                    discountstock.countproduct = newcountpro
                    discountstock.save()
                }
            }
            if($('#idgift').text() != ""){ 
                def searchcard = Card.findByIdcard($('#idgift').text())
                if(searchcard != null){
                    searchcard.delete()
                }
            }
            for(Tempsell temp : Tempsell.findAll()){
                if(session.user == temp.emid){
                    temp.delete()
                }
            }
            def runnum = new Runbill()
            runnum.numbill = Double.parseDouble($('#idbill').text())+1
            runnum.user = session.user
            runnum.save()
            redirect(uri: "/sell.zul")

        })  // จบเริ่มการขายสินค้าใหม่

        $('#printbill').on('click',{ // พิมพ์ใบเสร็จ
            
        }) // จบพิมพ์ใบเสร็จ
    }

    public void runbill(){
        Double maxnum = 1
        def findnumbill = Runbill.findByNumbill(maxnum)
        if(findnumbill == null){
            def runnum = new Runbill()
            runnum.numbill = maxnum
            runnum.user = session.user
            runnum.save()
            $('#idbill').val(maxnum)
        }
        else{
            for(Runbill run : Runbill.findAll()){
                if(maxnum <= run.numbill){
                    maxnum = run.numbill
                }
            }
            def findmax = Runbill.findByNumbill(maxnum)
            if(findmax != null){
                if(findmax.user != session.user){
                    maxnum++
                    def runnum = new Runbill()
                    runnum.numbill = maxnum
                    runnum.user = session.user
                    runnum.save()
                }
            }
            $('#idbill').val(maxnum)
        }
    }

    public String GetString(String str) {          
        return str
    } 

    public String getnumvat(){
        String str
        def numbervat = Infosetting.findAll()
        if(numbervat != null){
            str = numbervat.numvat
        }
        return str
    }

    public String dateandtime(){
        Date date = new Date()
        java.text.SimpleDateFormat df= new java.text.SimpleDateFormat();
        df.applyPattern("dd/MM/yyyy HH:mm:ss")
        return df.format(date)

    }

    public void pdfitext(){
    try{
            Rectangle pagesize = new Rectangle(210f, 297f)
            Document document = new Document(pagesize, 1.5f, 1.5f, 1.5f, 1.5f)
            LineSeparator line = new LineSeparator()

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(application.getRealPath("/ext/pdf/${session.user}"+$('#idbill').text()+".pdf")))

            document.open()

            BaseFont basef
            Font font


            basef = BaseFont.createFont(application.getRealPath("ARIALUNI.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
            font = new Font(basef, 6)
            String compa
            String addresscompa
            byte[] bytes 
            for(Infosetting checkdata : Infosetting.findAll()){
                if(checkdata != null){
                    compa = checkdata.namecompany
                    addresscompa = checkdata.addresscompany
                    bytes = checkdata.picture
                }
            }

            Paragraph companyname = new Paragraph(compa,font)
            companyname.setAlignment(Element.ALIGN_CENTER)
            Paragraph companyaddress = new Paragraph(addresscompa,font)
            companyaddress.setAlignment(Element.ALIGN_CENTER)
            Image img = Image.getInstance(bytes)
            img.setAlignment(Element.ALIGN_CENTER)
            img.scaleToFit(30,30)
            document.add(img)
            document.add(companyname)
            document.add(companyaddress)
            document.add(new Paragraph(GetString("พนักงาน : "+"${session.user}"),font))
            document.add(new Paragraph(GetString("รหัสบิล : "+$('#idbill').text()),font))
            document.add(new Paragraph(GetString("เลขประจำตัวผู้เสียภาษี : "+getnumvat()+"      "+dateandtime()),font))
            document.add(new Paragraph(" "))
            document.add(line)
            ColumnText ct = new ColumnText(writer.getDirectContent())
            for(Tempsell temp : Tempsell.findAll()){
                if(session.user == temp.emid){
                    def nameunit = Newstock.findByNameproduct(temp.nameproduct)
                    Paragraph left = new Paragraph(temp.nameproduct,font)
                    left.setAlignment(Element.ALIGN_LEFT)
                    Paragraph center = new Paragraph("${temp.countpro}"+" "+nameunit.nameunit,font)
                    center.setAlignment(Element.ALIGN_CENTER)
                    Paragraph right = new Paragraph("${temp.sum}",font)
                    right.setAlignment(Element.ALIGN_RIGHT)
                    document.add(left)
                    document.add(center)
                    document.add(right)
                }
            }
            document.add(new Paragraph(" "))
            document.add(line)
            Paragraph right1 = new Paragraph(GetString("Discount : " + $('#downpricefromlist').text()),font)
            right1.setAlignment(Element.ALIGN_LEFT)
            Paragraph right2 = new Paragraph(GetString("Total : "+$('#sumpricefromlist').text()),font)
            right2.setAlignment(Element.ALIGN_LEFT)
            Paragraph right3 = new Paragraph(GetString("Cash : "+session.cash),font)
            right3.setAlignment(Element.ALIGN_LEFT)
            Paragraph right4 = new Paragraph(GetString("Change : "+session.change),font)
            right4.setAlignment(Element.ALIGN_LEFT)
            Paragraph right5 = new Paragraph(GetString("Vat : "+$('#vatpricefromlist').text()),font)
            right5.setAlignment(Element.ALIGN_LEFT)
            document.add(right1)
            document.add(right2)
            document.add(right3)
            document.add(right4)
            document.add(right5)
            double temptotal = Double.parseDouble($('#sumpricefromlist').text().replace(",",""))
            for(Promotion checkpro : Promotion.findAll()){
                if (checkpro != null){
                    double pricepro = Double.parseDouble(checkpro.bathpro)
                    double totalperbathpro = temptotal/pricepro
                    double distpro = Double.parseDouble(checkpro.dispro)
                    SimpleDateFormat formatDateJava = new SimpleDateFormat("dd/MM/yyyy",new Locale("zh","CN"))
                    String dateext = formatDateJava.format(checkpro.extdaypro)
                    if(totalperbathpro >= 1){
                        int numcard = generratecard()
                        int disprice = totalperbathpro*distpro
                        Paragraph right6 = new Paragraph(GetString("รหัสส่วนลด " +"${numcard}"),font)
                        right6.setAlignment(Element.ALIGN_LEFT)
                        document.add(right6)
                        Paragraph right7 = new Paragraph(GetString("มูลค่า "+"${disprice}"+" บาท"),font)
                        right7.setAlignment(Element.ALIGN_LEFT)
                        document.add(right7)
                        Paragraph right8 = new Paragraph(GetString("หมดเขตรับสิทธิ์ "+"${dateext}"),font)
                        right8.setAlignment(Element.ALIGN_LEFT)
                        document.add(right8)
                        genpro(numcard,dateext,disprice)
                    }
                }
            }
            Paragraph footer = new Paragraph("*** VAT INCLUDED ***",font)
            footer.setAlignment(Element.ALIGN_CENTER)
            document.add(footer)
            document.close()
            $('#printbill').setHref("static/ext/pdf/${session.user}"+$('#idbill').text()+".pdf")
            $('#printbill').setTarget("_new")
            Thread.sleep(3000)
        }
        catch(Exception e){
            print(e)
            alert("เกิดข้อผิดพลาดในการสร้างใบเสร็จ")
        }
    }

    public genpro(int cardgen,String extdate,int bb){
        def addcard = new Card()
        addcard.idcard = cardgen
        addcard.nameadd = session.user
        addcard.impday = $('#imday').text()
        addcard.extday = extdate
        addcard.bath = bb
        addcard.save()
    }
    public int generratecard(){
        Random r = new Random()
        int num = r.nextInt(99999) + 99999
        if (num < 100000 || num > 999999) { 
            num = r.nextInt(99999) + 99999; 
            if (num < 100000 || num > 999999) { 
                throw new Exception("Unable to generate PIN at this time..")
            } 
        } 
        return num
    }

    public BigDecimal twodigit(Double num){
        BigDecimal twodi = new BigDecimal(num)
        twodi = twodi.setScale(2,BigDecimal.ROUND_HALF_EVEN)
        return twodi
    }

    public void printrowsell(){
        Double sumallm = 0
        int j = 1
        int checkbutton = 0
        String toonorsentstate = $('#toonorsent').selectedItem.label
        if(checkprin == true){
            $('#listbuy > rows > row').detach()
            checkprin = false
        }
        for(Tempsell temp : Tempsell.findAll()){
            if(temp.emid == session.user){
                $('#listbuy > rows').append{
                row{
                    label(value:"${j}",style:"font-size:14px;font-weight:bold;color:black")
                    label(value:temp.nameproduct,style:"font-size:14px;font-weight:bold;color:black")
                    label(value:"${temp.countpro}",style:"font-size:14px;font-weight:bold;color:black")
                    if(toonorsentstate == "[ขายปลีก]"){
                        label(value:"${temp.priceperunit}",style:"font-size:14px;font-weight:bold;color:black")
                        label(value:"${temp.sum}",style:"font-size:14px;font-weight:bold;color:black")
                        sumallm = sumallm+temp.sum
                        tempsumall = sumallm
                    }
                    if(toonorsentstate == "[ขายส่ง]"){
                        label(value:"${temp.pricebig}",style:"font-size:14px;font-weight:bold;color:black")
                        label(value:"${temp.pricebig * temp.countpro}",style:"font-size:14px;font-weight:bold;color:black")
                        sumallm = sumallm+(temp.pricebig*temp.countpro)
                        tempsumall = sumallm
                    }
                    button(id:temp.barcode,style:"font-size:14px;font-weight:bold;color:black",image:"/ext/images/icon/delete.png",mold:"trendy")
                    }
                    j++
                    checkprin = true
                }
                $('#listbuy > rows > row > button').on('click',{
                    for(Tempsell finddelete : Tempsell.findAll()){
                        if(finddelete.barcode == it.target.id && finddelete.emid == session.user){
                            finddelete.delete()
                            redirect(uri: "/sell.zul")
                        }
                    }
                        
                })
                vat = twodigit(sumallm-(sumallm/1.07))
                Double down = Double.parseDouble($('#downpricefromlist').text())
                vatornostate = $('#vatorno').selectedItem.label
                if(vatornostate == "[ไม่คิดVAT]"){
                    $('#vatpricefromlist').val("0")
                    $('#sumpricefromlist').val(tempsumall - vat - down)
                    $('#sumprice').val(tempsumall - vat - down)
                }
                if(vatornostate == "[คิดVAT]"){
                    $('#vatpricefromlist').val("${vat}")
                    $('#sumprice').val(tempsumall - down)
                    $('#sumpricefromlist').val($('#sumprice').text())
                }
                session.cash = ""
                session.change = ""
            }
        }
    }
}
